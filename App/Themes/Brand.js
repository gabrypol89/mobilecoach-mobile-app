// Brand Colors can be used to quickly adjust Look & Feel of the app.
// None of the Colors is used directly, they are just referenced in appColors and can be fully overwritten there.

/**
 * Changed some of the UI colors, the logo and the chat background.
 */

const brand = {
  colors: {
    primary: '#265EA3',
    primaryText: '#fff',
    buttonBackground: '#379683',
    buttonText: '#fff',
    backgroundMain: '#fff',
    textMain: '#3c3c3c',
    background1: '#f7f7f7',
    text1: '#2c3e50',
    background2: '#57BA98',
    text2: '#fff',
    grey1: '#525252',
    grey2: '#9B9B9B',
    grey3: '#F8F8F8'
  },
  images: {
    logo: require('../Images/app_logo_usz.png'),
    poweredBy: require('../Images/powered_by.png'),
    chatBackground: require('../Images/Chat/night.png')
  }
}

export default brand
